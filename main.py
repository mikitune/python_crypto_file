#coding:utf-8
import os
from cryptography.fernet import Fernet

def generate_key():
    """まだ存在しない場合は鍵を作成
    """    
    key = Fernet.generate_key()

    if not os.path.isfile('p.key'):
        with open('p.key', 'wb') as f:
            f.write(key)

def file2crypt(filepath):
    """パス指定したファイルを暗号化する
    Args:
        filepath (str): 暗号化するファイルのパス
    """    
    if not os.path.isfile('p.key'):
        generate_key()

    with open('p.key', 'rb') as f:
        key = f.read()

    with open(filepath, 'rb') as f:
        data = f.read()

    fernet    = Fernet(key)
    encrypted = fernet.encrypt(data)

    with open(f'{filepath}.crypted', 'wb') as f:
        f.write(encrypted)

def crypt2file(filepath):
    """パス指定した暗号化済みファイルを復号する
    Args:
        filepath (str): 暗号化済みファイルへのパス
    """    
    with open('p.key', 'rb') as f:
        key = f.read()

    with open(filepath, 'rb') as f:
        data = f.read()

    fernet    = Fernet(key)
    plaintext = fernet.decrypt(data)

    with open(f'{filepath}.plaintext', 'wb') as f:
        f.write(plaintext)

if __name__ == '__main__':
    file2crypt('test.txt')
    crypt2file('test.txt.crypted')